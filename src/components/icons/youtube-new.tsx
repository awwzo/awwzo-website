import * as React from "react";
import { SVGProps } from "react";
const SvgYoutubeNew = (props: SVGProps<SVGSVGElement>) => (
  <svg
    viewBox="0 0 26 19"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M25.463 2.849A3.257 3.257 0 0 0 23.172.557C21.137 0 12.999 0 12.999 0S4.86 0 2.826.536A3.323 3.323 0 0 0 .534 2.849C0 4.883 0 9.102 0 9.102s0 4.24.535 6.254a3.258 3.258 0 0 0 2.292 2.291c2.056.557 10.173.557 10.173.557s8.138 0 10.173-.535a3.257 3.257 0 0 0 2.292-2.292c.535-2.035.535-6.254.535-6.254s.021-4.24-.536-6.274Zm-15.055 10.15V5.205l6.767 3.898L10.408 13Z"
      fill="#FFF1DE"
    />
  </svg>
);
export default SvgYoutubeNew;
