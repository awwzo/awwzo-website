import * as React from "react";
import { SVGProps } from "react";
const SvgLinkedInNew = (props: SVGProps<SVGSVGElement>) => (
  <svg
    viewBox="0 0 26 26"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g fill="#FFF1DE">
      <path d="M26 26v-9.522c0-4.68-1.008-8.255-6.468-8.255-2.632 0-4.387 1.43-5.102 2.795h-.065V8.645H9.197V26h5.395v-8.612c0-2.275.423-4.453 3.218-4.453 2.762 0 2.795 2.568 2.795 4.583v8.45H26V26ZM.422 8.646h5.395v17.356H.422V8.646ZM3.12 0A3.12 3.12 0 0 0 0 3.12c0 1.723 1.398 3.153 3.12 3.153 1.723 0 3.12-1.43 3.12-3.153A3.12 3.12 0 0 0 3.12 0Z" />
    </g>
  </svg>
);
export default SvgLinkedInNew;
