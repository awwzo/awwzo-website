import * as React from "react";
import { SVGProps } from "react";
const SvgInstaNew = (props: SVGProps<SVGSVGElement>) => (
  <svg
    viewBox="0 0 26 26"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M19.5 0h-13C2.925 0 0 2.925 0 6.5v13C0 23.074 2.925 26 6.5 26h13c3.574 0 6.5-2.926 6.5-6.5v-13C26 2.925 23.073 0 19.5 0Zm4.333 19.5a4.338 4.338 0 0 1-4.333 4.333h-13A4.339 4.339 0 0 1 2.167 19.5v-13A4.338 4.338 0 0 1 6.5 2.167h13A4.338 4.338 0 0 1 23.833 6.5v13Zm-3.79-11.917a1.625 1.625 0 1 0 0-3.25 1.625 1.625 0 0 0 0 3.25ZM6.5 13a6.5 6.5 0 1 1 6.5 6.5A6.5 6.5 0 0 1 6.5 13Zm2.167 0a4.333 4.333 0 1 0 8.667 0 4.333 4.333 0 0 0-8.667 0Z"
      fill="#FFF1DE"
    />
  </svg>
);
export default SvgInstaNew;
