import * as React from "react";
import { SVGProps } from "react";
const SvgXNew = (props: SVGProps<SVGSVGElement>) => (
  <svg
    viewBox="0 0 26 27"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M15.473 11.251 25.152 0h-2.294l-8.404 9.77L7.74 0H0l10.15 14.773L0 26.57h2.294l8.875-10.316 7.089 10.316h7.742l-10.527-15.32Zm-3.142 3.652-1.028-1.471L3.119 1.727h3.523l6.604 9.446 1.029 1.471 8.584 12.28h-3.523l-7.005-10.02v-.001Z"
      fill="#FFF1DE"
    />
  </svg>
);
export default SvgXNew;
