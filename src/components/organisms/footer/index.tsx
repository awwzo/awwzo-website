import Link from "next/link";
import SvgLinkedInNew from "@/components/icons/linked-in-new";
import SvgInstaNew from "@/components/icons/insta-new";
import SvgYoutubeNew from "@/components/icons/youtube-new";
import SvgXNew from "@/components/icons/x-new";

const socialMediaItems = [
  {
    n: "LinkedIn",
    s: SvgLinkedInNew,
    h: "https://www.linkedin.com/company/awwzo",
  },
  {
    n: "Instagram",
    s: SvgInstaNew,
    h: "https://www.instagram.com/awwzohq/",
  },
  {
    n: "YouTube",
    s: SvgYoutubeNew,
    h: "https://www.youtube.com/@awwzohq",
  },
  { n: "Twitter (X)", s: SvgXNew, h: "https://twitter.com/@awwzohq" },
];

const Footer = () => {
  return (
    <section className="px-4 py-5 bg-branding-blue">
      <div className="max-w-[1500px] mx-auto flex space-x-8 items-center mdb:justify-center">
        {socialMediaItems.map(({ n, s, h }) => {
          const TagName = s;
          return (
            <Link
              key={n}
              href={h}
              target="_blank"
              className="group"
              aria-label={n}
            >
              <TagName className="w-[18px] md:w-[20px] group-hover:opacity-70" />
            </Link>
          );
        })}
      </div>
    </section>
  );
};

export default Footer;
