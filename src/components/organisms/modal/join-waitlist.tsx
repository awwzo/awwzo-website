import Image from "next/image";
import successPng from "@public/png/landing/awwSome.png";
import pawPrintBgPng from "@public/png/landing/paw_in_form.png";

const stopPropagation = (e: any) => e && e.stopPropagation();

const JoinWaitlistModal = ({ onDismiss }) => {
  return (
    <div
      className={`animate-fade fixed top-0 left-0 w-full h-full z-[100] flex items-end md:items-center justify-center`}
      style={{
        backdropFilter: `blur(4px)`,
      }}
      onClick={onDismiss}
    >
      <div
        className={`animate-fade-transform w-full max-w-[400px] flex flex-col items-center justify-center bg-white rounded-t-xl md:rounded-b-xl box-shadow-1`}
        onClick={stopPropagation}
      >
        <div className="relative w-full px-10 py-12">
          <Image
            src={pawPrintBgPng}
            alt=""
            className="absolute top-0 left-0 w-full h-full object-cover"
          />
          <div className="relative flex flex-col items-center justify-center">
            <Image src={successPng} alt="success" className="w-[185px]" />
            <p className="mt-6 text-body-3-medium md:text-title-1-medium text-black text-center">
              We&apos;ll Reach Out To You Soon!
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default JoinWaitlistModal;
