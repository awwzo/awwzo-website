import { ReactNode, useRef } from "react";
import { CSSTransition } from "react-transition-group";

const Animate = ({
  show,
  children,
}: {
  show: boolean;
  children: ReactNode;
}) => {
  const ref = useRef(null);

  return (
    <CSSTransition
      nodeRef={ref}
      in={show}
      timeout={300}
      classNames="animate"
      mountOnEnter
      unmountOnExit
    >
      <div ref={ref}>{children}</div>
    </CSSTransition>
  );
};

export default Animate;
