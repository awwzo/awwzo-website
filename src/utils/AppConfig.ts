export const AppConfig = {
  site_name: "Awwzo",
  title: "Awwzo",
  description: "Premium Pet Boarding",
  locale: "en",
  host: "https://awwzo.com",
};
