export const supportsWebm = () => {
  const testEl = document.createElement("video");
  return (
    testEl.canPlayType &&
    "" !== testEl.canPlayType('video/webm; codecs="vp8, vorbis"')
  );
};

export const supportsWebMAlpha = () => {
  return new Promise((resolve) => {
    const vid = document.createElement("video");
    vid.autoplay = false;
    vid.loop = false;
    vid.style.display = "none";
    vid.addEventListener(
      "loadeddata",
      () => {
        document.body.removeChild(vid);
        // Create a canvas element, this is what user sees.
        const canvas = document.createElement("canvas");

        //If we don't support the canvas, we definitely don't support webm alpha video.
        if (!(canvas.getContext && canvas.getContext("2d"))) {
          resolve(false);
          return;
        }

        // Get the drawing context for canvas.
        const ctx: any = canvas.getContext("2d");

        // Draw the current frame of video onto canvas.
        ctx.drawImage(vid, 0, 0);
        if (ctx.getImageData(0, 0, 1, 1).data[3] === 0) {
          resolve(true);
        } else {
          resolve(false);
        }
      },
      false
    );
    vid.addEventListener("error", () => {
      document.body.removeChild(vid);
      resolve(false);
    });

    vid.addEventListener("stalled", () => {
      document.body.removeChild(vid);
      resolve(false);
    });

    //Just in case
    vid.addEventListener("abort", () => {
      document.body.removeChild(vid);
      resolve(false);
    });

    const source = document.createElement("source");
    source.src =
      "data:video/webm;base64,GkXfowEAAAAAAAAfQoaBAUL3gQFC8oEEQvOBCEKChHdlYm1Ch4ECQoWBAhhTgGcBAAAAAAACBRFNm3RALE27i1OrhBVJqWZTrIHlTbuMU6uEFlSua1OsggEjTbuMU6uEHFO7a1OsggHo7AEAAAAAAACqAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVSalmAQAAAAAAADIq17GDD0JATYCNTGF2ZjU3LjU3LjEwMFdBjUxhdmY1Ny41Ny4xMDBEiYhARAAAAAAAABZUrmsBAAAAAAAARq4BAAAAAAAAPdeBAXPFgQGcgQAitZyDdW5khoVWX1ZQOYOBASPjg4QCYloA4AEAAAAAAAARsIFAuoFAmoECU8CBAVSygQQfQ7Z1AQAAAAAAAGfngQCgAQAAAAAAAFuhooEAAACCSYNCAAPwA/YAOCQcGFQAADBgAABnP///NXgndmB1oQEAAAAAAAAtpgEAAAAAAAAk7oEBpZ+CSYNCAAPwA/YAOCQcGFQAADBgAABnP///Vttk7swAHFO7awEAAAAAAAARu4+zgQC3iveBAfGCAXXwgQM=";
    source.addEventListener("error", () => {
      document.body.removeChild(vid);
      resolve(false);
    });
    vid.appendChild(source);

    //This is required for IE
    document.body.appendChild(vid);
  });
};
