import { Quicksand } from "next/font/google";
import localFont from "next/font/local";

export const quicksand = Quicksand({
  weight: ["500", "600"],
  subsets: ["latin"],
  variable: "--font-sans",
});

export const gellix = localFont({
  src: [
    {
      path: "../../public/fonts/Gellix-Regular.ttf",
      weight: "400",
      style: "normal",
    },
    {
      path: "../../public/fonts/Gellix-Medium.ttf",
      weight: "500",
      style: "normal",
    },
    {
      path: "../../public/fonts/Gellix-Bold.ttf",
      weight: "600",
      style: "normal",
    },
  ],
  variable: "--font-sans-2",
});

export const almondNougat = localFont({
  src: [
    {
      path: "../../public/fonts/Almond-Nougat.ttf",
      weight: "400",
      style: "normal",
    },
  ],
  variable: "--font-sans-3",
});
