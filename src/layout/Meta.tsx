import { NextSeo } from "next-seo";
import Head from "next/head";
import { useRouter } from "next/router";
import { AppConfig } from "@/utils/AppConfig";

type IMetaProps = {
  title?: string;
  description?: string;
  canonical?: string;
  images?: Array<any>;
};

const Meta = (props: IMetaProps) => {
  const router = useRouter();

  return (
    <>
      <Head>
        <meta charSet="UTF-8" key="charset" />
        <meta
          name="viewport"
          content="width=device-width,initial-scale=1,maximum-scale=5,viewport-fit=cover"
        />
        <meta name="keywords" content="pet boarding, pet, boarding" />

        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href={`${router.basePath}/apple-touch-icon.png`}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="512x512"
          href={`${router.basePath}/android-chrome-512x512.png`}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href={`${router.basePath}/android-chrome-192x192.png`}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href={`${router.basePath}/favicon-32x32.png`}
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href={`${router.basePath}/favicon-16x16.png`}
        />
        <link
          rel="icon"
          href={`${router.basePath}/favicon.ico`}
          key="favicon"
        />
        <link
          rel="mask-icon"
          href={`${router.basePath}/safari-pinned-tab.svg`}
          color="#5bbad5"
        />
        <link
          rel="manifest"
          href={`${router.basePath}/manifest.json`}
          crossOrigin="use-credentials"
        />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta
          name="msapplication-TileImage"
          content={`${router.basePath}/mstile-150x150.png`}
        />
        <meta name="theme-color" content="#212156" />
      </Head>

      <NextSeo
        title={props.title}
        description={props.description}
        canonical={`${AppConfig.host}${router.pathname}`}
        openGraph={{
          title: props.title,
          description: props.description,
          url: `${AppConfig.host}${router.pathname}`,
          locale: AppConfig.locale,
          site_name: AppConfig.site_name,
          images: [
            {
              url: `${AppConfig.host}/png/LOGO_.png`,
            },
          ],
        }}
        twitter={{
          cardType: "summary_large_image",
        }}
        robotsProps={{
          notranslate: true,
        }}
      />
    </>
  );
};

export { Meta };
