import type { AppProps } from "next/app";
import { quicksand, almondNougat, gellix } from "@/utils/fonts";
import "@/styles/globals.css";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <main
      className={`${quicksand.variable} ${almondNougat.variable} ${gellix.variable} font-sans-2`}
    >
      <Component {...pageProps} />
    </main>
  );
}
