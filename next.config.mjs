/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  distDir: "_next",
  trailingSlash: true,
  // experimental: {
  //   optimizeCss: true,
  // },
};

export default nextConfig;
