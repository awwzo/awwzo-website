import type { Config } from "tailwindcss";
const { fontFamily } = require("tailwindcss/defaultTheme");

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    screens: {
      sm: "640px",
      smb: { max: "640px" },
      md: "800px",
      mdb: { max: "800px" },
      lg: "1024px",
      lgb: { max: "1024px" },
      xl: "1280px",
      xlb: { max: "1280px" },
      "2xl": "1536px",
    },
    fontFamily: {
      sans: ["var(--font-sans)", ...fontFamily.sans],
      "sans-2": ["var(--font-sans-2)", "Trebuchet MS", ...fontFamily.sans],
      "sans-3": ["var(--font-sans-3)", "Brush Script MT", ...fontFamily.sans],
    },
    extend: {
      colors: {
        branding: {
          blue: "#212156",
          "blue-2": "#41418C",
          "blue-3": "#5757A1",
          yellow: "#E58329",
          "yellow-2": "#FF9D43",
          "yellow-3": "#FBBD08",
          "yellow-4": "#FDCB2E",
          "yellow-5": "#FFFCE9",
          "yellow-6": "#FFF1DE",
          "yellow-7": "#FFA858",
          "yellow-8": "#F5F0CF",
        },
        black: {
          950: "#000000",
          900: "#1E1C1F",
          600: "#656565",
          400: "#363636",
        },
        white: "#ffffff",
      },
      fontSize: {
        "ms-1-regular": ["0.5rem", { fontWeight: 400, lineHeight: "11.2px" }], // 8px
        "ms-1-medium": ["0.5rem", { fontWeight: 500, lineHeight: "11.2px" }], // 8px
        "ms-1-bold": ["0.5rem", { fontWeight: 600, lineHeight: "11.2px" }], // 8px
        "ms-2-regular": [
          "0.5625rem",
          { fontWeight: 400, lineHeight: "12.6px" },
        ], // 9px
        "ms-2-medium": ["0.5625rem", { fontWeight: 500, lineHeight: "12.6px" }], // 9px
        "ms-2-bold": ["0.5625rem", { fontWeight: 600, lineHeight: "12.6px" }], // 9px
        "ss-1-regular": ["0.625rem", { fontWeight: 500, lineHeight: "14px" }], // 10px
        "ss-1-bold": ["0.625rem", { fontWeight: 600, lineHeight: "14px" }], // 10px
        "ss-2-regular": ["0.75rem", { fontWeight: 400, lineHeight: "16.8px" }], // 12px
        "ss-2-medium": ["0.75rem", { fontWeight: 500, lineHeight: "16.8px" }], // 12px
        "ss-2-bold": ["0.75rem", { fontWeight: 600, lineHeight: "16.8px" }], // 12px
        "body-1-regular": [
          "0.875rem",
          { fontWeight: 400, lineHeight: "19.6px" },
        ], // 14px
        "body-1-medium": [
          "0.875rem",
          { fontWeight: 500, lineHeight: "19.6px" },
        ], // 14px
        "body-1-bold": ["0.875rem", { fontWeight: 600, lineHeight: "19.6px" }], // 14px
        "body-1-extra-bold": [
          "0.875rem",
          { fontWeight: 900, lineHeight: "19.6px" },
        ], // 14px
        "body-2-regular": ["1rem", { fontWeight: 400, lineHeight: "22.4px" }], // 16px
        "body-2-medium": ["1rem", { fontWeight: 500, lineHeight: "22.4px" }], // 16px
        "body-2-bold": ["1rem", { fontWeight: 600, lineHeight: "22.4px" }], // 16px
        "body-3-regular": [
          "1.125rem",
          { fontWeight: 400, lineHeight: "25.2px" },
        ], // 18px
        "body-3-medium": [
          "1.125rem",
          { fontWeight: 500, lineHeight: "25.2px" },
        ], // 18px
        "body-3-bold": ["1.125rem", { fontWeight: 600, lineHeight: "25.2px" }], // 18px
        "title-1-regular": ["1.25rem", { fontWeight: 400, lineHeight: "28px" }], // 20px
        "title-1-medium": ["1.25rem", { fontWeight: 500, lineHeight: "28px" }], // 20px
        "title-1-bold": ["1.25rem", { fontWeight: 600, lineHeight: "28px" }], // 20px
        "title-2-regular": [
          "1.5rem",
          { fontWeight: 400, lineHeight: "33.6px" },
        ], // 24px
        "title-2-bold": ["1.5rem", { fontWeight: 600, lineHeight: "33.6px" }], // 24px
        "title-2.5-regular": [
          "1.625rem",
          { fontWeight: 500, lineHeight: "31.2px" },
        ], // 26px
        "title-3-regular": [
          "1.75rem",
          { fontWeight: 400, lineHeight: "33.6px" },
        ], // 28px
        "title-3": ["1.75rem", { fontWeight: 500, lineHeight: "33.6px" }], // 28px
        "title-4": ["2rem", { fontWeight: 600, lineHeight: "32px" }], // 32px
        "title-4.5": ["2.5rem", { fontWeight: 400, lineHeight: "35.28px" }], // 40px
        "title-5": ["3rem", { fontWeight: 400, lineHeight: "40.32px" }], // 48px
        "title-6": ["3.25rem", { fontWeight: 600, lineHeight: "72.8px" }], // 52px
        "title-7": ["3.5rem", { fontWeight: 600, lineHeight: "78.4px" }], // 56px
        "title-8": ["3.75rem", { fontWeight: 600, lineHeight: "84px" }], // 60px
        "title-9": ["4rem", { fontWeight: 400, lineHeight: "57.12px" }], // 64px
        "title-10": ["5rem", { fontWeight: 600, lineHeight: "112px" }], // 80px
        "title-11": ["5rem", { fontWeight: 600, lineHeight: "120.4px" }], // 86px
        "title-12": ["5rem", { fontWeight: 400, lineHeight: "75px" }], // 80px
        "title-13": ["5.625rem", { fontWeight: 400, lineHeight: "75.6px" }], // 90px
        "title-14": ["6.75rem", { fontWeight: 400, lineHeight: "84px" }], // 108px
      },
      transitionProperty: {
        width: "width",
        height: "height",
      },
    },
  },
  plugins: [],
};
export default config;
